<?php

require_once "conexion.php";

class ModeloDisciplinas{

    /*=============================================
	CREAR DISCIPLINAS
	=============================================*/

    static public function mdlIngresarDisciplina($tabla, $datos){

        $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(disciplina) VALUES (:disciplina)");

        $stmt->bindParam(":disciplina", $datos, PDO::PARAM_STR);

        if ($stmt -> execute()) {
            
            return "ok";

        }else{

            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    /*=============================================
	MOSTRAR DISCIPLINAS
	=============================================*/

    static public function mdlMostrarDisciplinas($tabla, $item, $valor){

        if ($item != null) {
            
            $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

            $stmt->bindParam(":".$item, $valor, PDO::PARAM_STR);

            $stmt ->execute();

            return $stmt->fetch();

        }else{

            $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

            $stmt->execute();

            return $stmt->fetchAll();

        }

        $stmt->close();
        $stmt=null;
    }

    /*=============================================
	EDITAR DISCIPLINAS
	=============================================*/

    static public function mdlEditarDisciplina($tabla, $datos){

        $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET disciplina=:disciplina WHERE id = :id");

        $stmt->bindParam(":disciplina", $datos["disciplina"], PDO::PARAM_STR);
        $stmt->bindParam(":id", $datos["id"], PDO::PARAM_INT);

        if ($stmt -> execute()) {
            
            return "ok";

        }else{

            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    /*=============================================
	BORRAR DISCIPLINAS
	=============================================*/

    static public function mdlBorrarDisciplina($tabla, $datos){

        $stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id = :id");

        $stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

        if ($stmt -> execute()) {
            
            return "ok";

        }else{

            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

}