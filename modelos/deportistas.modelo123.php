<!-- paso 10 (crearDeportista)-> requiere conexion.php-->

<?php

require_once "conexion.php";
//paso 9 (crearDeportista)-> se crea clase ModeloDeportistas
class ModeloDeportistas{  

    /*=============================================
	CREAR DEPORTISTA
	=============================================*/
    //paso 11 (crearDeportista)-> metodo estatico mdlIngresarDepoortista que recibe datos $tabla, $datos
    static public function mdlIngresarDeportista($tabla, $datos){
        ////paso 12 (crearDeportista)->preparacion sql de insertar 
        $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(nombres, apellidos, ci, nacimiento, telefono, direccion, altura, peso, email) VALUES (:nombres, :apellidos, :ci, :nacimiento, :telefono, :direccion, :altura, :peso, :email)");
        ////paso 13 (crearDeportista)->enlasamos parametros de cada uno de losc ampóps que vienen en datos

        $stmt->bindParam(":nombres", $datos["nombres"], PDO::PARAM_STR);
        $stmt->bindParam(":apellidos", $datos["apellidos"], PDO::PARAM_STR);
		$stmt->bindParam(":ci", $datos["ci"], PDO::PARAM_INT);
		$stmt->bindParam(":nacimiento", $datos["nacimiento"], PDO::PARAM_STR);
		$stmt->bindParam(":telefono", $datos["telefono"], PDO::PARAM_STR);
		$stmt->bindParam(":direccion", $datos["direccion"], PDO::PARAM_STR);
		$stmt->bindParam(":altura", $datos["altura"], PDO::PARAM_STR);
		$stmt->bindParam(":peso", $datos["peso"], PDO::PARAM_STR);
		$stmt->bindParam(":email", $datos["email"], PDO::PARAM_STR);

        //paso 14 (crearDeportista)-> SI se ejecuta correctamente retornara ok

        if ($stmt->execute()) {
            
            return "ok";

        }else{

            return "error";
        }

        $stmt->close();
        $stmt = null;
    }
    //paso5 MostrarDEPORTISTAS 
    /*=============================================
	MOSTRAR DEPORTISTAS
	=============================================*/

    static public function mdlMostrarDeportistas($tabla, $item, $valor){
        //paso6 MostrarDEPORTISTAS->preguntamos si viene a nulo  para devolver el item a lo que venga en los parametros item y valor
        if ($item != null) {
            
            $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

            $stmt->bindParam(":".$item, $valor, PDO::PARAM_STR);

            $stmt ->execute();

            //paso7 MostrarDEPORTISTAS->devolvemos con fetch una sola fila

            return $stmt->fetch();

        }else{
            //paso8 MostrarDEPORTISTAS->si viene vacio retornamos fechall para que traiga todos los deportistas
            $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

            $stmt->execute();

            return $stmt->fetchAll();

        }
        //paso9 MostrarDEPORTISTAS-> cerramos conexion y vaciamos objeto PDO
        $stmt->close();
        $stmt=null;
    }

    /*=============================================
	EDITAR DEPORTISTAS
	=============================================*/

    static public function mdlEditarDeportista($tabla, $datos){

        $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET nombres=:nombres, apellidos=:apellidos, ci=:ci, nacimiento=:nacimiento, telefono=:telefono, direccion=:direccion, altura=:altura, peso=:peso, email=:email WHERE id = :id");

        $stmt->bindParam(":id", $datos["id"], PDO::PARAM_INT);
        $stmt->bindParam(":nombres", $datos["nombres"], PDO::PARAM_STR);
        $stmt->bindParam(":apellidos", $datos["apellidos"], PDO::PARAM_STR);
		$stmt->bindParam(":ci", $datos["ci"], PDO::PARAM_INT);
		$stmt->bindParam(":nacimiento", $datos["nacimiento"], PDO::PARAM_STR);
		$stmt->bindParam(":telefono", $datos["telefono"], PDO::PARAM_STR);
		$stmt->bindParam(":direccion", $datos["direccion"], PDO::PARAM_STR);
		$stmt->bindParam(":altura", $datos["altura"], PDO::PARAM_STR);
		$stmt->bindParam(":peso", $datos["peso"], PDO::PARAM_STR);
		$stmt->bindParam(":email", $datos["email"], PDO::PARAM_STR);

        if ($stmt -> execute()) {
            
            return "ok";

        }else{

            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    /*=============================================
	ELIMINAR DEPORTISTAS
	=============================================*/

	static public function mdlEliminarDeportista($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id = :id");

		$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();
		$stmt = null;
	}

}