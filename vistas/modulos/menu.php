<aside class="main-sidebar ">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li >
                <a href="inicio">
                    <i class="fa fa-home"></i>
                    <span>Inicio</span>
                </a>
            </li>
            <li>
                <a href="usuarios">
                    <i class="fa fa-user"></i>
                    <span>Usuarios</span>
                </a>
            </li>
            <li>
                <a href="atletas">
                    <i class="fa fa-user"></i>
                    <span>atletas</span>
                </a>
            </li>
            <li>
                <a href="disciplinas">
                    <i class="fa fa-th"></i>
                    <span>Disciplinas</span>
                </a>
            </li>
            <li>
                <a href="reportes">
                    <i class="fa fa-print"></i>
                    <span>Reportes</span>
                </a>
            </li>
 
        </ul>
    </section>
</aside>