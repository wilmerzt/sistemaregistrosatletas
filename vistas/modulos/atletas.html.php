<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Administrar deportistas
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="active">Administrar deportistas</li>
    
    </ol> 

  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">
  
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarDeportista">
          
          Agregar Deportista

        </button>

      </div>

      <div class="box-body">
        
       <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
         
        <thead>
         
         <tr>
           
           <th style="width:10px">#</th>
           <th>Nombres</th>
           <th>Apellidos</th>
           <th>Documento CI</th>
           <th>Nacionalidad</th>
           <th>Fecha nacimiento</th>
           <th>Teléfono</th>
           <th>Dirección</th>          
           <th>Altura</th>
           <th>Peso</th>
           <th>Email</th>

         </tr> 

        </thead>

        <tbody>
          
        <?php

            $item = null;
            $valor = null;
            $deportistas = ControladorDeportistas::ctrMostrarDeportistas($item, $valor);


            foreach ($deportistas as $key => $value) {
              
              echo '<tr>

              <td>'.($key+1).'</td>

              <td>'.$value["nombres"].'</td>
              <td>'.$value["apellidos"].'</td>
              <td>'.$value["ci"].'</td>
              <td>'.$value["nacimiento"].'</td>
              <td>'.$value["telefono"].'</td>
              <td>'.$value["direccion"].'</td>
              <td>'.$value["altura"].'</td>
              <td>'.$value["peso"].'</td>
              <td>'.$value["email"].'</td>';
            // btnEditarDdeportista-> captura datos con javascript idDeportista="'.$value["id"].'" -> atributo
              echo'<td>

                <div class="btn-group">
              
                  <button class="btn btn-warning btnEditarDeportista" idDeportista="'.$value["id"].'" data-toggle="modal" data-target="#modalEditarDeportista"> <i class="fa fa-pencil"> Editar</i></button>

                  <button class="btn btn-danger btnEliminarDeportista" idDeportista="'.$value["id"].'"><i class="fa fa-times"> Eliminar</i></button>

                </div>  

              </td>

            </tr>';
            }
            ?>    

        </tbody>

       </table>

      </div>

    </div>

  </section>

</div>

<!--=====================================
MODAL AGREGAR DEPORTISTA
======================================-->

<div id="modalAgregarDeportista" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#001a57; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Agregar Deportista</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <!-- ENTRADA PARA LOS NOMBRES-->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                <input type="text" class="form-control input-lg" name="nuevaNombres" placeholder="Ingresar Nombres" required>

              </div>

            </div>

            <!-- ENTRADA PARA LOS APELLIDOS-->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                <input type="text" class="form-control input-lg" name="nuevaApellidos" placeholder="Ingresar apellidos" required>

              </div>

            </div>


            <!-- ENTRADA PARA CARNET DE IDENTIDAD-->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-align-left"></i></span> 

                <input type="text" class="form-control input-lg" name="nuevaCi" placeholder="Ingresar numero de cedula de identidad" required>

              </div>

            </div>   
            

            <!-- ENTRADA PARA FECHA DE NACIMIENTO-->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span> 

                <input type="date" class="form-control input-lg" name="nuevaNacimiento"  required>

              </div>

            </div>

            <!-- ENTRADA PARA TELEFONO-->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-phone"></i></span> 

                <input type="number" class="form-control input-lg" name="nuevaTelefono" placeholder="Ingresar numero de telefono" required>

              </div>

            </div>


            <!-- ENTRADA PARA DIRECCION-->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span> 

                <input type="text" class="form-control input-lg" name="nuevaDireccion" placeholder="Ingresar Direccion" required>

              </div>

            </div>


            <!-- ENTRADA PARA ALTURA-->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-sort"></i></span> 

                <input type="number" step="any" class="form-control input-lg" name="nuevaAltura" placeholder="Ingresar altura" required>

              </div>

            </div>

            <!-- ENTRADA PARA PESO-->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-sort"></i></span> 

                <input type="number"  step="any" class="form-control input-lg" name="nuevaPeso" placeholder="Ingresar PESO" required>

              </div>

            </div>


            <!-- ENTRADA PARA EMAIL-->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span> 

                <input type="email" class="form-control input-lg" name="nuevaEmail" placeholder="Ingresar correo electronico" required>

              </div>

            </div>

          </div>

         </div> 
        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Guardar Deportista</button>

        </div>

      </form>

    </div>

  </div>

</div>