<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Administrar Disciplinas
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="active">Administrar Disciplinas</li>
    
    </ol>

  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">
  
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarDisciplina">
          
          Agregar disciplina

        </button>

      </div>

      <div class="box-body">
        
       <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
         
        <thead>
         
         <tr>
           
           <th style="width:10px">#</th>
           <th>Categoria</th>
           <th>acciones</th>

         </tr> 

        </thead>

        <tbody>


        <?php

            $item = null;
            $valor = null;
            $disciplinas = ControladorDisciplinas::ctrMostrarDisciplinas($item, $valor);


            foreach ($disciplinas as $key => $value) {
              # code...
              echo '<tr>

              <td>'.($key+1).'</td>
  
              <td class="text-uppercase">'.$value["disciplina"].'</td>';
            // btnEditarDisciplina-> captura datos con javascript idDisciplina="'.$value["id"].'" -> atributo
              echo'<td>
  
                <div class="btn-group">
               
                  <button class="btn btn-warning btnEditarDisciplina" idDisciplina="'.$value["id"].'" data-toggle="modal" data-target="#modalEditarDisciplina"> <i class="fa fa-pencil"></i></button>
  
                  <button class="btn btn-danger btnEliminarDisciplina" idDisciplina="'.$value["id"].'"><i class="fa fa-times"></i></button>
  
                </div>  
  
              </td>
  
            </tr>';
            }
        ?>  
          
        </tbody>

       </table>

      </div>

    </div>

  </section>

</div>

<!--=====================================
MODAL AGREGAR DISCIPLINA
======================================-->

<div id="modalAgregarDisciplina" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#001a57; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Agregar Disciplina</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <!-- ENTRADA PARA EL NOMBRE -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-th"></i></span> 

                <input type="text" class="form-control input-lg" name="editarDisciplina" placeholder="Ingresar disciplina" required>

              </div>

            </div>

          </div>

         </div> 
        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Guardar Disciplina</button>

        </div>
        
        <?php

          $crearDisciplina = new ControladorDisciplinas();
          $crearDisciplina -> ctrCrearDisciplina();
        
        ?>

      </form>

    </div>

  </div>

</div>

<!--=====================================
MODAL EDITAR DISCIPLINA
======================================-->

<div id="modalEditarDisciplina" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#001a57; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Editar Disciplina</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <!-- ENTRADA PARA EL NOMBRE -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-th"></i></span> 

                <input type="text" class="form-control input-lg" name="editarDisciplina"  id="editarDisciplina" required>
                   <!-- transparente porque si no hace ninguncambio se queda con el miso valor y se queda desde javascript -->
                <input type="hidden" name="idDisciplina" id="idDisciplina"  required> 

              </div>

            </div>

          </div>

         </div> 
        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Guardar Cambios</button>

        </div>
        
        <?php

          $editarDisciplina = new ControladorDisciplinas();
          $editarDisciplina -> ctrEditarDisciplina();
        
        ?>

      </form>

    </div>

  </div>

</div>



<?php

  $borrarDisciplina = new ControladorDisciplinas();
  $borrarDisciplina -> ctrBorrarDisciplina();

?>