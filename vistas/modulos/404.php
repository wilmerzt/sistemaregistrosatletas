<div class="content-wrapper">

    <section class="content-header">
      <h1>
        Pagina no encontrada

      </h1>
      <ol class="breadcrumb">
        <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>

        <li class="active">Pagina no encontrada</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

    <div class="content">
        <div class="error-page">
            <h2 class="headline text-primary">404</h2>
            <div class="error-content">
                <h3><i class="fa fa-warning text-primary"></i>
                Ooops! Pagina No encontrada.
            </h3>
            <p>
                Ingrese al menu lateral y alli podrás   encontrar las páginas disponibles. <br>
                Tambien puedes regresar haciendo <a href="inicio">Clic aqui.</a>
            </p>
            </div>
        </div>    
    </div>
    </section>
    <!-- /.content -->
  </div>