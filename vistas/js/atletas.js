/*=============================================
EDITAR ATLETA
=============================================*/
$(".tablas").on("click", ".btnEditarAtleta", function(){

	var idAtleta = $(this).attr("idAtleta");

	var datos = new FormData();
    datos.append("idAtleta", idAtleta);

    $.ajax({

      url:"ajax/atletas.ajax.php",
      method: "POST",
      data: datos,
      cache: false,
      contentType: false,
      processData: false,
      dataType:"json",
      success:function(respuesta){
      
      	   $("#idAtleta").val(respuesta["id"]);
	       $("#editarAtleta").val(respuesta["nombre"]);
	       $("#editarDocumentoId").val(respuesta["documento"]);
	       $("#editarEmail").val(respuesta["email"]);
	       $("#editarTelefono").val(respuesta["telefono"]);
	       $("#editarDireccion").val(respuesta["direccion"]);
           $("#editarFechaNacimiento").val(respuesta["fecha_nacimiento"]);
           $("#editarAltura").val(respuesta["altura"]);
           $("#editarPeso").val(respuesta["peso"]);
	  } 

  	})

})

/*============================================= 
ELIMINAR ATLETA
=============================================*/
$(".tablas").on("click", ".btnEliminarAtleta", function(){

	var idAtleta = $(this).attr("idAtleta");
	
	swal({
        title: '¿Está seguro de borrar el atleta?',
        text: "¡Si no lo está puede cancelar la acción!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borrar atleta!'
      }).then(function(result){
        if (result.value) {
          
            window.location = "index.php?ruta=atletas&idAtleta="+idAtleta;
        }

  })

})