/*=============================================
EDITAR DISCIPLINA
=============================================*/
$(".tablas").on("click", ".btnEditarDisciplina", function(){

	var idDisciplina = $(this).attr("idDisciplina");
	
	var datos = new FormData();

	datos.append("idDisciplina", idDisciplina);

	$.ajax({

		url:"ajax/disciplinas.ajax.php",
		method: "POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success: function(respuesta){

            // console.log("respuesta", respuesta);

            $("#editarDisciplina").val(respuesta["disciplina"]);
            $("#idDisciplina").val(respuesta["id"]); // jalamos de diciplinas imput hidedn
		}

	})

})

/*=============================================
ELIMINAR DISCIPLINA
=============================================*/
$(".tablas").on("click", ".btnEliminarDisciplina", function(){

	var idDisciplina = $(this).attr("idDisciplina");
  
	swal({
	  title: '¿Está seguro de borrar la disciplina?',
	  text: "¡Si no lo está puede cancelar la accíón!",
	  type: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		cancelButtonText: 'Cancelar',
		confirmButtonText: 'Si, borrar Disciplina!'
	}).then(function(result){
  
	  if(result.value){
  
		window.location = "index.php?ruta=disciplinas&idDisciplina="+idDisciplina;
  
	  }
  
	})



})
