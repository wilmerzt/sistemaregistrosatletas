<?php

class ControladorAtletas{

	/*=============================================
	CREAR ATLETA
	=============================================*/

	static public function ctrCrearAtleta(){

		if(isset($_POST["nuevoAtleta"])){

			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nuevoAtleta"]) &&
			   preg_match('/^[0-9]+$/', $_POST["nuevoDocumentoId"]) &&
			   preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["nuevoEmail"]) && 
			   preg_match('/^[()\-0-9 ]+$/', $_POST["nuevoTelefono"]) && 
			   preg_match('/^[#\.\-a-zA-Z0-9 ]+$/', $_POST["nuevaDireccion"])){

			   	$tabla = "atletas";

			   	$datos = array("nombre"=>$_POST["nuevoAtleta"],
					           "documento"=>$_POST["nuevoDocumentoId"],
					           "email"=>$_POST["nuevoEmail"],
					           "telefono"=>$_POST["nuevoTelefono"],
					           "direccion"=>$_POST["nuevaDireccion"],
					           "fecha_nacimiento"=>$_POST["nuevaFechaNacimiento"],
					           "altura"=>$_POST["nuevaAltura"],
					           "peso"=>$_POST["nuevoPeso"]);

			   	$respuesta = ModeloAtletas::mdlIngresarAtleta($tabla, $datos);

			   	if($respuesta == "ok"){

					echo'<script>

					swal({
						  type: "success",
						  title: "El atleta ha sido guardado correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "atletas";

									}
								})

					</script>';

				}

			}else{

				echo'<script>

					swal({
						  type: "error",
						  title: "¡El atleta no puede ir vacío o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "atletas";

							}
						})

			  	</script>';



			}

		}

	}

	/*=============================================
	MOSTRAR ATLETAS
	=============================================*/

	static public function ctrMostrarAtletas($item, $valor){

		$tabla = "atletas";

		$respuesta = ModeloAtletas::mdlMostrarAtletas($tabla, $item, $valor);

		return $respuesta;

	}

	/*=============================================
	EDITARATLETA
	=============================================*/

	static public function ctrEditarAtleta(){
 
		if(isset($_POST["editarAtleta"])){

			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["editarAtleta"]) &&
			   preg_match('/^[0-9]+$/', $_POST["editarDocumentoId"]) &&
			   preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["editarEmail"]) && 
			   preg_match('/^[()\-0-9 ]+$/', $_POST["editarTelefono"]) && 
			   preg_match('/^[#\.\-a-zA-Z0-9 ]+$/', $_POST["editarDireccion"])){

			   	$tabla = "atletas";

			   	$datos = array("id"=>$_POST["idAtleta"],
			   				   "nombre"=>$_POST["editarAtleta"],
					           "documento"=>$_POST["editarDocumentoId"],
					           "email"=>$_POST["editarEmail"],
					           "telefono"=>$_POST["editarTelefono"],
					           "direccion"=>$_POST["editarDireccion"],
					           "fecha_nacimiento"=>$_POST["editarFechaNacimiento"],
					           "altura"=>$_POST["editarAltura"],
					           "peso"=>$_POST["editarPeso"]);

			   	$respuesta = ModeloAtletas::mdlEditarAtleta($tabla, $datos);

			   	if($respuesta == "ok"){ 

					echo'<script>

					swal({
						  type: "success",
						  title: "El atleta ha sido cambiado correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "atletas";

									}
								})

					</script>';

				}

			}else{

				echo'<script>

					swal({
						  type: "error",
						  title: "¡El atleta no puede ir vacío o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "atletas";

							}
						})

			  	</script>';



			}

		}

	}

	/*=============================================
	ELIMINAR ATLETA
	=============================================*/

	static public function ctrEliminarAtleta(){

		if(isset($_GET["idAtleta"])){

			$tabla ="atletas";
			$datos = $_GET["idAtleta"];

			$respuesta = ModeloAtletas::mdlEliminarAtleta($tabla, $datos);

			if($respuesta == "ok"){

				echo'<script>

				swal({
					  type: "success",
					  title: "El atleta ha sido borrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "atletas";

								}
							})

				</script>';

			}		

		}

	}

}

