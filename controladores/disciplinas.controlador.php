<?php

class ControladorDisciplinas{

    /*=============================================
	CREAR DISCIPLINAS
	=============================================*/

    static public function ctrCrearDisciplina(){ 

        if(isset($_POST["nuevaDisciplina"])){

            if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nuevaDisciplina"])){

                $tabla = "disciplinas";

                $datos = $_POST["nuevaDisciplina"];

                $respuesta = ModeloDisciplinas::mdlIngresarDisciplina($tabla, $datos);

                if ($respuesta == "ok") {

					echo '<script>

					swal({

						type: "success",
						title: "¡La disciplina ha sido guardada correctamente!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar",
                        closeOnConfirm: false
					}).then((result) => {

						if(result.value){
						
							window.location = "disciplinas";

						}

					})
				

					</script>';
                }

            }else{

            echo '<script>

                swal({

                    type: "error",
                    title: "¡La disciplina no puede ir vacía o llevar caracteres especiales!",
                    showConfirmButton: true,
                    confirmButtonText: "Cerrar",
                    closeOnConfirm: false
                    }).then((result) =>{

                        if(result.value){
                            
                            window.location = "disciplinas";

                        }
                  });

            </script>';
            }

        }

    }

    /*=============================================
	MOSTRAR DISCIPLINAS
	=============================================*/

    static public function ctrMostrarDisciplinas($item, $valor){

        $tabla = "disciplinas";

        $respuesta = ModeloDisciplinas::mdlMostrarDisciplinas($tabla, $item, $valor);

        return $respuesta;
    }

    /*=============================================
	EDITAR DISCIPLINAS
	=============================================*/

    static public function ctrEditarDisciplina(){

        if(isset($_POST["editarDisciplina"])){

            if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["editarDisciplina"])){

                $tabla = "disciplinas";

                $datos = array("disciplina"=>$_POST["editarDisciplina"],
                               "id"=>$_POST["idDisciplina"]);

                $respuesta = ModeloDisciplinas::mdlEditarDisciplina($tabla, $datos);

                if ($respuesta == "ok") {

					echo'<script>

					swal({
						  type: "success",
						  title: "La disciplina ha sido cambiada correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "disciplinas";

									}
								})

					</script>';

				}

            }else{

				echo'<script>

					swal({
						  type: "error",
						  title: "¡La disciplina no puede ir vacía o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "disciplinas";

							}
						})

			  	</script>';

			}

        }

    }

    /*=============================================
	BORRAR DISCIPLINAS
	=============================================*/
    static public function ctrBorrarDisciplina(){

        if(isset($_GET["idDisciplina"])){


                $tabla = "disciplinas";

                $datos = $_GET["idDisciplina"];

                $respuesta = ModeloDisciplinas::mdlBorrarDisciplina($tabla, $datos);

                if ($respuesta == "ok") {

					echo'<script>

					swal({
						  type: "success",
						  title: "La disciplina ha sido borrada correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "disciplinas";

									}
								})

					</script>';

            }

        }

    }

}