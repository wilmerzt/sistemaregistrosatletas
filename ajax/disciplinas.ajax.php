<?php

require_once "../controladores/disciplinas.controlador.php";
require_once "../modelos/disciplinas.modelo.php";

class AjaxDisciplinas{

    /***************************************
     EDITAR DISCIPLINA
     ***************************************/

    public $idDisciplina;
//no colocamos static porque en el servidor actual sale error con static
    public function ajaxEditarDisciplina(){

        $item = "id";

        $valor = $this -> idDisciplina;

        $respuesta=ControladorDisciplinas::ctrMostrarDisciplinas($item,$valor);

        echo json_encode($respuesta);

    }
}

/***************************************
     EDITAR DISCIPLINA
 ***************************************/

 if (isset($_POST["idDisciplina"])) {
     
    $disciplina = new AjaxDisciplinas();

    $disciplina -> idDisciplina = $_POST["idDisciplina"];

    $disciplina -> ajaxEditarDisciplina();
 }