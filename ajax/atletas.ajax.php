<?php

require_once "../controladores/atletas.controlador.php";
require_once "../modelos/atletas.modelo.php";

class AjaxAtletas{ 

	/*=============================================
	EDITAR ATLETA
	=============================================*/	

	public $idAtleta;

	public function ajaxEditarAtleta(){

		$item = "id";
		$valor = $this->idAtleta;

		$respuesta = ControladorAtletas::ctrMostrarAtletas($item, $valor);

		echo json_encode($respuesta);

		

	} 

}

/*=============================================
EDITAR ATLETA
=============================================*/	

if(isset($_POST["idAtleta"])){

	$atleta = new AjaxAtletas();
	$atleta -> idAtleta = $_POST["idAtleta"];
	$atleta -> ajaxEditarAtleta();

}
